var defaulter = require('../defaulter');
var assert = require('assert');

describe('Defaulter', function() {
	var Defaulter = defaulter.Defaulter;

    it('works like in the example', function() {
       var defaulter = Defaulter({name: 'you', title: 'Mrs.'});
       var options = defaulter({name: 'Smith'});
       var greeting = "Hello, " + options.title + " " + options.name;
       assert.equal(greeting, 'Hello, Mrs. Smith');
	});
	it('throws when instantiated with no defaults', function() {
		assert.throws(function() { Defaulter(); }, Error);
	});
	it('throws when instantiated with empty defaults', function() {
		assert.throws(function() { Defaulter({}); }, Error);
	});
	it('defaults when no value is given for one key', function() {
		var defaulter = Defaulter({a: 3});
		var result = defaulter({});
		assert.equal(result.a, 3); 
	});
	it('does not default when a value is given for one key' , function() {
		var defaulter = Defaulter({a: 3});
		var result = defaulter({a: 4});
		assert.equal(result.a, 4);
	});
	it('gives all defaults when no values are given', function() {
		var defaulter = Defaulter({a: 3, b: 4});
		result = defaulter();
		assert.equal(result.a, 3);
		assert.equal(result.b, 4);
	});
	it('gives passed value and default together', function() {
		var defaulter = Defaulter({a: 3, b: 4});
		var result = defaulter({a: 10});
		assert.equal(result.a, 10);
		assert.equal(result.b, 4);
	});
	it('gives no defaults when all values are given', function() {
		var defaulter = Defaulter({a: 3, b: 4});
		var result = defaulter({a: 4, b: 5});
		assert.equal(result.a, 4);
		assert.equal(result.b, 5);
	});
});
