/* A poor man's keyword argument with default.
 *
 * Make a function which, when called with an object containing certain keys,
 * returns a different object containing the passed keys or else certain
 * default values. e.g.:
 *
 *     var defaulter = Defaulter({name: 'you', title: 'Mrs.'});
 *     var options = defaulter({name: 'Smith'});
 *     var greeting = "Hello, " + options.title + " " + options.name;
 *     assert.equal(greeting, 'Hello, Mrs. Smith');
 *
 */
function Defaulter(defaults) {
    "use strict";
    if (!(this instanceof Defaulter)) {
        return new Defaulter(defaults);
    }
    if (!defaults) {
        throw new Error("missing defaults; cannot provide any values");
    }
    var property, empty = true;
    for (property in defaults) {
        if (defaults.hasOwnProperty(property)) {
            empty = false;
            break;
        }
    }
    if (empty) {
        throw new Error("empty defaults; cannot provide any values");
    }
    var defaulter = function(options) {
        // For each property defined on defaults, but not options, use the
        // defaults value. For each own property defined on options otherwise,
        // just use its value.
        var result = {};
        var option_value;
        var property;
        for (property in defaults) {
            if (defaults.hasOwnProperty(property)) {
                result[property] = defaults[property];
            }
        }
        for (property in options) {
            if (defaults.hasOwnProperty(property)) {
                option_value = options[property];
                if (typeof(option_value) == 'undefined') {
                    result[property] = defaults[property];
                }
                else {
                    result[property] = option_value;
                }
            }
            else {
                throw new Error("unexpected option " + property);
            }
        }
        return result;
    };
    return defaulter;
}

module.exports = {
    Defaulter: Defaulter
};
